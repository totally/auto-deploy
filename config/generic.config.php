<?php
return array(
    'application' => array(
        'email' => array(
            'fromName' => 'AutoDeploy',
            'fromEmail' => 'test@test.com',
            'replyTo' => 'test@test.com',
            'ccEmail' => [], // add any email addresses to be CC'd on all deployment notifications
        )
    ),

    'auto_deploy' => array(
        /**
         * @todo allow for multiple of each service type
         */
        'services' => array(
            'vcs' => array(
                'type' => 'git',
                'branch' => 'master', // This is the git branch that you wish to auto deploy
                'runAs' => '', // user to execute vcs commands
                'originUrl' => ''
            ),

            'dm' => array(
                'type' => 'composer',
                'runAs' => '', // user to execute dm commands
                'name' => '' // This is required to identify the correct composer.json file
            ),

            'db' => array(
                'type' => 'mysql',
                'runAs' => '', // user to execute db commands
                'migrationDir' => '', // This is where the db migration files are kept relative to the vcs root
                // This is where the backup taken prior to a db migration are kept relative to the vcs root
                // make sure this directory is excluded from version control
                'backupDir' => '',
                // the below is required to perform db migrations - this is the target db
                'connection' => array(
                    'hostname' => '',
                    'username' => '',
                    'password' => '',
                    'database' => ''
                ),
                // the below is required to perform db migrations - multiple databases can be backed up if required
                'backup_connections' => array(
                    /*array(
                        'hostname' => '',
                        'username' => '',
                        'password' => '',
                        'database' => '',
                        'skipTables' => [],
                    ),*/
                ),
            ),
        ),

        // This is a list of white-listed IP addresses for the modules internal firewall
        'ipAddresses' => array(
            // add ip addresses
        ),

        'log' => array(
            'enabled' => true,
            'logger' => 'Zend\Log\Logger', // default use the Zend Logger (must be implement Zend\Log\LogInterface)
            'logDir' => 'var/log', // directory that the log file lives in
            'logFile' => 'application.log', // log file name
            'logTitle' => 'AutoDeploy', // log entry title
            'mail' => true,
            'mailerClass' => 'AutoDeploy\Application\SystemEmail', // default use the Zend Logger (must be implement AutoDeploy\Application\SystemEmailInterface)
        ),

        /**
         * Hooks provide a way to run command line executables or specific php commands
         * via call_user_func_array().  For each hook, specify the 'callback' and 'params' which will be passed
         * straight into call_user_func_array()
         *
         * Example hooks:
         *  array('\My\Worker\Class::methodToRun', array($param1, $param2)),
         *  array('exec', 'find vendor/ -type d chmod 0775 {} +'),
         *  array('exec', 'find vendor/ -type d chmod 0664 {} +'),
         */
        'hooks' => array(
            'preUpdate' => array(
                // hooks to run before composer runs
            ),
            'postUpdate' => array(
                // hooks to run after composer runs
            )
        ),

        // default timeout (in seconds) for running auto deployments
        'timeout' => 600
    ),
);