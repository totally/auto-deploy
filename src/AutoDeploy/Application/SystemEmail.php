<?php
/**
 * @package   AutoDeploy
 *
 * @copyright 2014 Totally Communications (http://www.totallycommunications.com)
 * @license   http://www.totallycommunications.com/license/bsd.txt New BSD License
 * @version   $Id:$
 */
namespace AutoDeploy\Application;

class SystemEmail implements SystemEmailInterface
{
    public $fromName = '';
    public $fromEmail = '';

    public $ccEmail;
    public $replyTo;

    public function __construct(array $emailConfig = array())
    {
        foreach ($emailConfig as $field => $value) {
            $this->{$field} = $value;
        }
    }

    /**
     * Send email
     *
     * @param string $toEmail Email address to send email to
     * @param string $subject Subject of email
     * @param string $content Content of email
     * @param string $fromEmail Email address email should come from,
     *               if not supplied will use system default
     * @param string $fromName Name to appear in from email address,
     *               if not supplied will use system default
     * @param $attachments[] file attachment path
     * @return void
     */
    public function send($toEmail = null, $subject = null, $content = null,
        $fromEmail = null, $fromName = null, $attachments = null
    )  {

        // get from email address
        $fromEmail = ($fromEmail) ? $fromEmail : $this->fromEmail;
        $fromName  = ($fromName) ? $fromName : $this->fromName;

        $mail = new \Zend_Mail();
        $tr = new \Zend_Mail_Transport_Sendmail('-f' . $fromEmail);
        \Zend_Mail::setDefaultTransport($tr);

        $mail->setFrom($fromEmail, $fromName);
        $mail->setSubject(stripslashes($subject));

        // make HTML email content
        $html_content = $this->htmlContent($content);

        // make text email content
        $text_content = strip_tags($html_content);

        $mail->setBodyText($text_content);
        $mail->setBodyHtml($html_content);

        if (is_array($toEmail)) {
            foreach ($toEmail as $email) {
                $this->addEmail($mail, $email);
            }
        } else {
            $this->addEmail($mail, $toEmail);
        }

        if (is_array($this->ccEmail)) {
            foreach($this->ccEmail as $email) {
                $mail->addCc($email);
            }
        } else if($this->ccEmail) {
            $mail->addCc($this->ccEmail);
        }

        $mail->setReturnPath($this->replyTo);

        if (!$mail->send()) {
            return false;
        }
        return true;
    }

    /**
     * Returns HTML content of email
     *
     * @param string $content Content of email
     * @return string HTML content
     */
    protected function htmlContent($content)
    {
        // make HTML email content
        $htmlContent = $this->html_header();
        $htmlContent.= nl2br($content);
        $htmlContent.= $this->html_footer();

        return $htmlContent;
    }

    /**
     * Returns header part of email template
     *
     * @return string HTML content
     */
    protected function html_header()
    {
        $html = '
            <html>
            <body bgcolor="#FFFFFF" style="margin:0;padding:0">
                <style type="text/css" title="text/css">
                    body { font-family: Arial,"DejaVu Sans","Liberation Sans",Freesans,sans-serif; font-size: 13px; color: #000; line-height: 22px; }
                    a { color: #CC0000; text-decoration: none; }
                    /* Hack hotmail */
                    .ReadMsgBody {width: 100%;}
                    .ExternalClass {width: 100%;}
                </style>
                <center>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" bgcolor="#FFFFFF">
                        <tr>
                            <td valign="top" align="center">
                                <table align="center" cellpadding="0" cellspacing="0" border="0" width="650" bgcolor="#FFFFFF">
                                    <tr>
                                        <td style="padding: 30px 15px; text-align: left; font-family: Arial,\'DejaVu Sans\',\'Liberation Sans\',Freesans,sans-serif; font-size: 13px; color: #000; line-height: 22px;">';


        return $html;
    }

    /**
     * Returns footer part of email template
     *
     * @return string HTML content
     */
    protected function html_footer()
    {

        $html = '
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </body>
            </html>';

        return $html;
    }

    /**
     * Proxy method to $message->addTo($email). Make sure, if we're on dev
     * environment that only authorises email addresses are added.
     *
     * @param string $email
     *
     * @return void
     */
    protected function addEmail($mail, $email)
    {
        $mail->addTo($email);
    }
}
